export interface Autonomo {
    nivelAutomatizacion: number;
    precisionGPS: number;
    camaras: float;
    machineLearning: boolean;
    navegacionInercial: boolean;
}